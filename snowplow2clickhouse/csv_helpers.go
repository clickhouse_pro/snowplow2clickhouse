package snowplow2clickhouse

import (
	"bytes"
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"bitbucket.org/clickhouse_pro/components/stacktrace"
	"github.com/rs/zerolog/log"
)

func refreshCSVShemaWithFormKey(s *Snowplow2Clickhouse, k string) bool {
	s.formKeyExistsMap[k] = true
	s.dbFieldList = append(s.dbFieldList, snowplowMap[k])
	s.dbToFormMap[snowplowMap[k]] = k
	return true
}

func refreshCSVShemaWithJSONKey(s *Snowplow2Clickhouse, k string) bool {
	s.jsonKeyExistsMap[k] = true
	s.dbFieldList = append(s.dbFieldList, k)
	s.dbToFormMap[k] = k
	return true
}

// nolint: gas
func (s *Snowplow2Clickhouse) initCSVWriter() {
	s.csvFileName = fmt.Sprintf(
		"%s/collect/%s.%d.csv", s.dataDir, time.Now().UTC().Format("2006-01-02__15-04-05.999999999"), s.schemaVersion,
	)

	csvFile, err := os.OpenFile(s.csvFileName, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
	s.csvFile = csvFile
}

// nolint: gas
func (s *Snowplow2Clickhouse) writeCSVHead() {
	head := fmt.Sprintf(
		"INSERT INTO %s.%sevents_local (%s) FORMAT CSV\n", s.database, s.tablePrefix, strings.Join(s.dbFieldList, ","),
	)
	if _, err := s.csvFile.WriteString(head); err != nil {
		log.Fatal().Err(err).Msg("writeCSVHead error")
	}
}

// see http://stackoverflow.com/questions/43436121/howto-write-uuid-raw-16-bytes-to-csv-in-golang
// nolint: gocyclo
func (s *Snowplow2Clickhouse) writeCSVRow(form url.Values) {
	var err error
	row := s.prepareCSVRow(form)

	const doubleQuote uint8 = 34
	var bDoubleQoute = []byte{doubleQuote}
	var bDoubleQouteQuoted = []byte{doubleQuote, doubleQuote}

	const singleQuote uint8 = 39
	var bSingleQuote = []byte{singleQuote}

	const commaASCII uint8 = 44
	var bComma = []byte{commaASCII}

	const newLine uint8 = 10
	var bNewLine = []byte{newLine}

	const endLine uint8 = 13
	var bEndLine = []byte{endLine}

	for i, b := range row {
		hasQuote := bytes.Contains(b, bDoubleQoute) || bytes.Contains(b, bSingleQuote)
		hasComma := bytes.Contains(b, bComma)
		hasNewline := bytes.Contains(b, bNewLine) || bytes.Contains(b, bEndLine)
		if hasQuote {
			b = bytes.Replace(b, bDoubleQoute, bDoubleQouteQuoted, -1)
		}
		if hasComma || hasQuote || hasNewline {
			b = append(bDoubleQoute, append(b, doubleQuote)...)
		}
		_, err = s.csvFile.Write(b)
		if err == nil && i < len(row)-1 {
			_, err = s.csvFile.Write(bComma)
		}
		if err != nil {
			break
		}
	}

	if err == nil {
		_, err = s.csvFile.Write(bNewLine)
	}

	if err != nil {
		log.Error().Err(err).Msg("writeCSVRow error")
	}

	s.writedRows++
}

func (s *Snowplow2Clickhouse) prepareCSVRow(form url.Values) [][]byte {
	dbLen := len(s.dbFieldList)
	row := make([][]byte, dbLen)
	for i := 0; i < dbLen; i++ {
		dbField := s.dbFieldList[i]
		if formKey, formExists := s.dbToFormMap[dbField]; formExists {
			formValue := form.Get(formKey)
			if dbType, exists := dbFieldTypes[dbField]; exists {
				row[i] = formatToDbType(formValue, dbType)
			} else {
				row[i] = []byte(formValue)
			}
		}
	}

	return row
}
