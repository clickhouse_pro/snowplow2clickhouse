# Installation local development environment
* Install https://golang.org/dl/, https://www.virtualbox.org/wiki/Downloads, https://www.vagrantup.com/downloads.html and your favorite IDE with http://editorconfig.org/ support

* Setup GOPATH and GOROOT environment variables

* Run following command
```bash
git clone git@bitbucket.org:clickhouse_pro/snowplow2clickhouse.git $GOPATH/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse
cd $GOPATH/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse
cp -fv ~/.ssh/id_rsa $GOPATH/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse/id_rsa
chmod -v 0600 $GOPATH/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse/id_rsa
vagrant up --provsion
```

# Simple usage

```bash
vagrant ssh
systemctl start clickhouse-server
cd ~/go/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse
go run main.go
```