package snowplow2clickhouse

import (
	"log"
	"net/http"
	"testing"

	"bitbucket.org/clickhouse_pro/components/browscap"
	"github.com/gocraft/web"
)

type testResponseWriter struct {
	web.ResponseWriter
}

func (w testResponseWriter) Write(data []byte) (n int, err error) {
	return len(data), nil
}

func (w testResponseWriter) Header() http.Header {
	return http.Header{}
}

func createTestCollector() *Snowplow2Clickhouse {
	s := NewSnowplow2ClickHouse()
	s.dropDatabase = true
	s.chunkSize = 1000
	s.dataDir = "/tmp/data"
	s.geoipDir = "/var/lib/GeoIP"
	s.database = "snowplow2clickhouse_test"
	s.clusterName = "snowplow2clickhouse_test"
	s.tablePrefix = ""
	s.clickhouseHosts = []string{"vagrant.snowplow2clickhouse.clickhouse.pro:8123"}
	s.CreateDataDir()
	s.InitGeoip()
	browscap.InitBrowsCap()
	s.InitClickHouseClient()
	s.CreateClickHouseTables()
	s.fillIgluPrefixesMap()
	s.SpawnProcessRequest()
	return s
}

func TestSnowplow2ClickHouse_CollectSequencedGET(t *testing.T) {
	collectOnce(newTestRequestOldProtocol)
}

func TestSnowplow2ClickHouse_CollectSequencedPOST(t *testing.T) {
	collectOnce(newTestRequestJSONPayload)
}

func collectOnce(testedRequest func() web.Request) {
	s := createTestCollector()
	req := testedRequest()
	rw := web.ResponseWriter(testResponseWriter{})
	s.collect(rw, &req)
	s.ShutdownCollector()
}

func BenchmarkSnowplow2ClickHouse_CollectSequencedGET(b *testing.B) {
	log.Printf("GET SEQUENCED b.N=%d", b.N)
	runSequence(b, newTestRequestOldProtocol)
}

func BenchmarkSnowplow2ClickHouse_CollectSequencedPOST(b *testing.B) {
	log.Printf("POST SEQUENCED b.N=%d", b.N)
	runSequence(b, newTestRequestJSONPayload)
}

func runSequence(b *testing.B, testedRequest func() web.Request) {
	s := createTestCollector()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		req := testedRequest()
		rw := web.ResponseWriter(testResponseWriter{})
		s.collect(rw, &req)
	}
	s.ShutdownCollector()

}

func BenchmarkSnowplow2ClickHouse_CollectParallelGET(b *testing.B) {
	log.Printf("GET PARALLEL b.N=%d", b.N)
	runParallel(b, newTestRequestOldProtocol)
}

func BenchmarkSnowplow2ClickHouse_CollectParallelPOST(b *testing.B) {
	log.Printf("POST PARALLEL b.N=%d", b.N)
	runParallel(b, newTestRequestJSONPayload)
}

func runParallel(b *testing.B, testedRequest func() web.Request) {
	s := createTestCollector()
	defer s.ShutdownCollector()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		rw := web.ResponseWriter(testResponseWriter{})

		for pb.Next() {
			req := testedRequest()
			s.collect(rw, &req)
		}
	})

}
