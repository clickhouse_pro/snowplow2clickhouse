package snowplow2clickhouse

import (
	"bytes"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"github.com/gocraft/web"
	"github.com/satori/go.uuid"
)

func TestParseGeoip(t *testing.T) {
	s := NewSnowplow2ClickHouse()
	s.geoipDir = "/var/lib/GeoIP"
	s.InitGeoip()
	req := web.Request{
		Request: &http.Request{},
	}
	req.Form = url.Values{}

	for _, ip := range []string{"80.249.80.187", "2001:470:0:76::2", "2001:470:1f1d:275::7"} {
		req.RemoteAddr = ip
		if parseGeoip(&req, s) == nil {
			t.Errorf("%s not found", ip)
		}
	}
	for _, ip := range []string{"", "10.10.10", "127.0.0.1", "2001:", "192.168.1.1"} {
		req.RemoteAddr = ip
		lookup := parseGeoip(&req, s)
		if lookup != nil {
			t.Errorf("%s wrong found %v", ip, lookup)
		}
	}
	s.geoip.Close()
}

func TestFormatToDbType(t *testing.T) {

	if !bytes.Equal(formatToDbType("", "Int16"), []byte("0")) {
		t.Fatalf("formatToDbType(\"\",\"Int16\")=%s\n", formatToDbType("", "Int16"))
	}
	if !bytes.Equal(formatToDbType("", "UUID"), []byte(strings.Repeat("0", 16))) {
		t.Fatalf("formatToDbType(\"\",\"UUID\")=%s\n", formatToDbType("", "UUID"))
	}
	for _, s := range []string{"e1393c62-877a-4adc-8ffb-f1bf0a337c5f", "74576fb6-a6fb-40d9-a2f6-f84c739a1f15"} {
		v, err := uuid.FromString(s)
		if err != nil {
			t.Fatal(err)
		}
		if !bytes.Equal(formatToDbType(v.String(), "UUID"), v.Bytes()) || len(formatToDbType(v.String(), "UUID")) != 16 {
			t.Fatalf("formatToDbType(\"%s\",\"UUID\") != %s\n", v.String(), formatToDbType(v.String(), "UUID"))
		}
	}
}
