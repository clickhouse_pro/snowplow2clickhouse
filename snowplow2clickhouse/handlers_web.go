package snowplow2clickhouse

import (
	"errors"
	"fmt"
	"html"
	"io/ioutil"
	"strings"

	"github.com/gocraft/web"
	"github.com/rs/zerolog/log"
)

var pixelGIF = []byte{71, 73, 70, 56, 57, 97, 1, 0, 1, 0, 144, 0, 0, 255, 0, 0, 0, 0, 0, 33, 249, 4, 5, 16, 0, 0, 0, 44, 0, 0, 0, 0, 1, 0, 1, 0, 0, 2, 2, 4, 1, 0, 59}

func (s *Snowplow2Clickhouse) collect(rw web.ResponseWriter, req *web.Request) {
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Content-Type", "image/gif")
	err := req.ParseForm()
	if err == nil {
		if req.Header.Get("Content-Type") == "application/json" && len(req.Form) == 0 && req.ContentLength > 0 {
			parseUserAgent(req)
			parseGeoip(req, s)
			parseUTM(req)
			parseJSONBody(req, s)
		} else if len(req.Form) > 0 {
			parseUserAgent(req)
			parseGeoip(req, s)
			parseUTM(req)
			parseJSONForm(req.Form, s)
		} else {
			body, bodyErr := ioutil.ReadAll(req.Body)
			if bodyErr == nil {
				logErrorToFile(s, body, errors.New("invalid request body"))
			} else {
				logErrorToFile(s, body, bodyErr)
			}

		}
	} else {
		log.Error().Err(err).Msg("req.ParseForm error")
	}

	if _, err = rw.Write(pixelGIF); err != nil {
		log.Error().Err(err).Msg("rw.Write(pixelGIF) error")
	}
}

func (s *Snowplow2Clickhouse) showExample(rw web.ResponseWriter, req *web.Request) {
	rw.Header().Set("Content-Type", "text/html; charset=utf-8")
	counterCode := `
<html>
<head>
 <title>snowplow2clickhouse example</title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
 <!-- Snowplow 2.7.0 javascript tracker see https://github.com/snowplow/snowplow/wiki/Javascript-Tracker -->
 <script type="text/javascript" async=1>
    ;(function(p,l,o,w,i,n,s){if(!p[i]){p.GlobalSnowplowNamespace=p.GlobalSnowplowNamespace||[];
    p.GlobalSnowplowNamespace.push(i);p[i]=function(){(p[i].q=p[i].q||[]).push(arguments)
    };p[i].q=p[i].q||[];n=l.createElement(o);s=l.getElementsByTagName(o)[0];n.async=1;
    n.src=w;s.parentNode.insertBefore(n,s)}}(window,document,"script","//d1fc8wv8zag5ca.cloudfront.net/2.7.0/sp.js","snowplow"));
 </script>
</head>
<body bgcolor="#FFFFFF">
<script>
    snowplow('newTracker', 'snowplow2clickhouse_get', '//` + req.Host + `/snowplow2clickhouse.collect', { // Initialise a tracker
      appId: 'pro.clickhouse',
      post: false,
      cookieDomain: 'clickhouse.pro',
      contexts: {
            webPage: true,
            gaCookies: true,
            performanceTiming: true,
            geolocation: true
      }
    });

    snowplow('newTracker', 'snowplow2clickhouse_post', '//` + req.Host + `/snowplow2clickhouse.collect', { // Initialise a tracker
      appId: 'pro.clickhouse',
      post: true,
      cookieDomain: 'clickhouse.pro',
      contexts: {
            webPage: true,
            gaCookies: true,
            performanceTiming: true,
            geolocation: true
      }
    });

    snowplow("enableFormTracking");
    snowplow("enableLinkClickTracking", null, true, true);
    snowplow("enableActivityTracking", 10, 10);
    snowplow('enableErrorTracking');

    snowplow('trackPageView');

    snowplow('addItem',
        'T1',                                              // order ID - required
        'snowplow2clickhouse_subscription',                // SKU/code - required
        'Snowplow 2 Clickhouse collector subscription',    // product name
        'Software',                                        // category or variation
        '29.99',                                           // unit price - required
        '1',                                               // quantity - required
        'USD'                                             // currency
    );

    snowplow('addTrans',
        'T1',           // order ID - required
        'Cloudpayments - subscribe',  // affiliation or store name
        '11.99',          // total - required
        '1.29',           // tax
        '5',              // shipping
        'Chelyabinsk',       // city
        'South Ural',     // state
        'Russia',           // country
        'USD'			// currency
    );

    snowplow('trackTrans');
    snowplow('trackSocialInteraction', 'like', 'facebook', 'first_public');

    snowplow('trackAdImpression',
        '67965967893',            // impressionId
        'cpa',                    // costModel - 'cpa', 'cpc', or 'cpm'
        10,                       // cost - requires costModel
        'http://www.example.com', // targetUrl
        '23',                     // bannerId
        '7',                      // zoneId
        '201',                    // advertiserId
        '12'                      // campaignId
    );

    snowplow('trackAdClick',
        'http://www.example.com',      // targetUrl
        '12243253',                    // clickId
        'cpm',                         // costModel
        0.5,                           // cost
        '23',                          // bannerId
        '7',                           // zoneId
        '67965967893',                 // impressionId - the same as in trackAdImpression
        '201',                         // advertiserId
        '12'                           // campaignId
    );

    snowplow('trackAdConversion',
       '743560297',        // conversionId
        'cpm',             // costModel
        10,                // cost
        'ecommerce',       // category
        'purchase',        // action
        '',                // property
        99,                // initialValue - how much the conversion is initially worth
        '201',             // advertiserId
        '12'               // campaignId
    );

    snowplow('trackStructEvent',
        'category','action','label','property','value'
    );
    // self describing JSON unstructured event
    //   https://github.com/snowplow/snowplow/wiki/2-Specific-event-tracking-with-the-Javascript-tracker#311-trackaddtocart-and-trackremovefromcart
    snowplow('trackAddToCart',
        '000345', //SKU
        'snowplow2clickhouse_subscription', // product name
        'Software', //category
        19.99, // Unit Price
        2, // Quantity
        'USD' // Currency
    );
    snowplow('trackRemoveFromCart',
        '000345', //SKU
        'snowplow2clickhouse_subscription', // product name
        'Software', //category
        19.99, // Unit Price
        2, // Quantity
        'USD' // Currency
    );

    snowplow('trackSiteSearch',
        ['shut up', 'and take', 'my money'],      // search terms
        {'category': 'pricing', 'sub-category': 'where is checkout page?'},  // filters
        14,                      // results found
        8                        // results displayed on first page
    );

    snowplow('trackTiming',
        'load',            // Category of the timing variable
        'map_loaded',      // Variable being recorded
        50,                // Milliseconds taken
        'Map loading time' // Optional label
    );

    snowplow('addEnhancedEcommerceActionContext',
        'T2', // The Transaction ID
        'Cloudpayments', // The affiliate
        '19.99', // The revenue
        '2.85', // The tax
        '5.34', // The shipping
        'COUPON_EXAMPLE' // The coupon
    );

    snowplow('addEnhancedEcommerceImpressionContext',
        'snowplow2clickhouse_subscription', // The SKU / Product ID
        'Snowplow To Clickhouse Collector', // The name
        'Search Results', // The list
        'Clickhouse.pro', // The brand
        'Software', // The category
        'Subscription', // The variant
        '1' // The position
    );

    snowplow('addEnhancedEcommerceProductContext',
        'snowplow2clickhouse_subscription', // The SKU / Product ID
        'Snowplow To Clickhouse Collector', // The name
        'Search Results', // The list
        'Clickhouse.pro', // The brand
        'Software', // The category
        'Subscription', // The variant
        '1' // The position
    );

    snowplow('addEnhancedEcommercePromoContext',
        'PROMO_1234', // The Promotion ID
        'Summer Sale', // The name
        'summer_banner2', // The name of the creative
        'banner_slot1' // The position
    );

    snowplow('trackEnhancedEcommerceAction',
      'detail'
    );
    snowplow('trackEnhancedEcommerceAction',
      'purchase'
    );

    try {
        throw { message: "Some", code: 403 }
    } catch(e) {
        snowplow('trackError',
            'Error happens? Are you kidding?', // message
            'filename.js', // file
            0, // line
            0, // column
            e // error
        );
    }

</script>
<h1>Your first snowplow2clickhouse example</h1>
{{snippet}}
</body>
</html>
    `

	counterCode = strings.Replace(
		counterCode,
		"{{snippet}}",
		"<pre class=\"prettyprint\"><code class=\"language-html\">"+html.EscapeString(counterCode)+"</code></pre>",
		1,
	)

	fmt.Fprint(rw, counterCode)
}
