module bitbucket.org/clickhouse_pro/snowplow2clickhouse

go 1.13

require (
	bitbucket.org/clickhouse_pro/components v0.0.0-20180208070017-8c55b42c9de7
	github.com/buger/jsonparser v0.0.0-20181115193947-bf1c66bbce23
	github.com/digitalcrab/browscap_go v0.0.0-20160912072603-465055751e36
	github.com/gocraft/web v0.0.0-20190207150652-9707327fb69b
	github.com/kr/pretty v0.1.0 // indirect
	github.com/oschwald/maxminddb-golang v1.4.0
	github.com/roistat/go-clickhouse v1.0.1
	github.com/rs/zerolog v1.15.0
	github.com/satori/go.uuid v1.2.0
	github.com/sebest/xff v0.0.0-20160910043805-6c115e0ffa35
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v1.1.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
