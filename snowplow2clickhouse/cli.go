package snowplow2clickhouse

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/clickhouse_pro/components/stacktrace"
	"bitbucket.org/clickhouse_pro/components/gocraft_stacktrace"
	"github.com/gocraft/web"
	"github.com/oschwald/maxminddb-golang"
	"github.com/roistat/go-clickhouse"
)

//ParseCmdlineArgs use -help for usage
func (s *Snowplow2Clickhouse) ParseCmdlineArgs() {
	var clickhouseHosts string
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.PanicOnError)
	//@todo DEV/2h need integrate to Zookeeper API for grab all click house hosts from current cluster
	flag.StringVar(&clickhouseHosts, "clickhouseHosts", "localhost:8123", "Comma separated clickhouse hosts")
	flag.StringVar(&s.dataDir, "dataDir", "./data", "Directory for data processing")
	flag.StringVar(&s.listen, "listen", ":8321", "Listen directive")
	flag.BoolVar(&s.dropDatabase, "dropDatabase", false, "Drop and create database")
	flag.StringVar(&s.database, "database", "snowplow2clickhouse", "Clickhouse database name")
	flag.StringVar(&s.tablePrefix, "tablePrefix", "", "Clickhouse tables prefix")
	flag.StringVar(&s.clusterName, "clusterName", "snowplow2clickhouse", "Clickhouse cluster name for distributed tables")
	flag.StringVar(&s.geoipDir, "geoipDir", "/var/lib/GeoIP/", "Directory with maxmind geoip database files")
	flag.StringVar(&s.igluPrefixes, "igluPrefixes", "", "Iglu Prefixes format prefix1=http://url1/path1,prefix2=http://url2/path2")
	flag.UintVar(&s.chunkSize, "chunkSize", 10000, "Flush data to clickhouse every X requests")
	flag.Parse()

	s.fillIgluPrefixesMap()
	s.clickhouseHosts = make([]string, strings.Count(clickhouseHosts, ",")+1)
	s.clickhouseHosts = strings.Split(clickhouseHosts, ",")
	geoipDir, err := filepath.Abs(s.geoipDir)
	if err != nil {
		log.Fatalf("%s not exists", s.geoipDir)
	}
	s.geoipDir = geoipDir
	log.Print("Command Line parameters parsed")
}

//InitClickHouseClient use roistat/go-clickhouse http client
func (s *Snowplow2Clickhouse) InitClickHouseClient() {
	transport := clickhouse.NewHttpTransport()
	s.clickhouseConns = make([]*clickhouse.Conn, len(s.clickhouseHosts))
	for i, host := range s.clickhouseHosts {
		s.clickhouseConns[i] = clickhouse.NewConn(host, transport)
	}

	s.clickhouse = clickhouse.NewCluster(s.clickhouseConns...)
	s.clickhouse.OnCheckError(func(c *clickhouse.Conn) {
		log.Printf("Clickhouse connection failed %s", c.Host)
	})
	go func() {
		for {
			s.clickhouse.Check()
			time.Sleep(time.Minute)
		}
	}()
	log.Print("ClickHouse Cluster Initialized successful")
}

//CreateClickHouseTables see types.go for table definition
func (s *Snowplow2Clickhouse) CreateClickHouseTables() {
	queries := make([]string, 0)
	if s.dropDatabase {
		queries = append(queries, "DROP DATABASE IF EXISTS "+s.database)
	}
	createQueries := []string{
		"CREATE DATABASE IF NOT EXISTS " + s.database,
		fmt.Sprintf(
			createMainEventsTableTemplate,
			s.database+"."+s.tablePrefix+"events_local",
			"ENGINE = MergeTree(date_created, (app_id, date_created), 8192)",
		),
		fmt.Sprintf(
			createMainEventsTableTemplate,
			s.database+"."+s.tablePrefix+"events_distributed",
			"ENGINE = Distributed("+s.clusterName+", "+s.database+", "+s.tablePrefix+"events_local)",
		),
	}
	queries = append(queries, createQueries...)

	q := clickhouse.NewQuery("")
	lenCon := len(s.clickhouseConns)
	lenQ := len(queries)

	for j := 0; j < lenQ; j++ {
		for i := 0; i < lenCon; i++ {
			q.Stmt = queries[j]
			if err := q.Exec(s.clickhouseConns[i]); err != nil {
				stacktrace.DumpErrorStackAndExit(err)
			}
		}
	}

	log.Printf(
		"Create ClickHouse cluster='%s' database='%s' tablePrefix='%s' dropDatabase=%t COMPLETE",
		s.clusterName, s.database, s.tablePrefix, s.dropDatabase,
	)
}

//CreateDataDir create subdirectories for collected csv files
// nolint: gas
func (s *Snowplow2Clickhouse) CreateDataDir() {
	createDirIfNotExists(s.dataDir, 0755)
	createDirIfNotExists(s.dataDir+"/process", 0755)
	createDirIfNotExists(s.dataDir+"/collect", 0755)
	createDirIfNotExists(s.dataDir+"/archive", 0755)
	createDirIfNotExists(s.dataDir+"/errors", 0755)
	log.Print("Data dicretories created")
}

//InitGeoip load MaxMind GeoIP database via memory map interface
func (s *Snowplow2Clickhouse) InitGeoip() {
	file := s.geoipDir + "/GeoLite2-City.mmdb"
	geoip, err := maxminddb.Open(file)
	if err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	} else {
		log.Printf("GEOIP %s opened", file)
	}
	s.geoip = geoip
}

//InitRouterAndListWebsever gocraft/web initialization
//@TODO 8h/DEV maybe think about https://github.com/gramework/gramework
func (s *Snowplow2Clickhouse) InitRouterAndListWebsever() {
	router := web.New(*s).
		Middleware(web.LoggerMiddleware).
		Middleware(gocraft_stacktrace.LogErrorMiddleware).
		Get("/snowplow_js_example.html", s.showExample).
		Get("/snowplow2clickhouse.collect/:*", s.collect).
		Post("/snowplow2clickhouse.collect/:*", s.collect)

	log.Printf("InitRouterAndListWebsever BEGIN listen=%s", s.listen)
	if err := http.ListenAndServe(s.listen, router); err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
	log.Print("InitRouterAndListWebsever END")
}

//ShutdownCollector wait all unprocessed data and
func (s *Snowplow2Clickhouse) ShutdownCollector() {
	log.Printf("ShutdownCollector BEGIN g.writedRows=%d", s.writedRows)
	close(s.bufferedData)
	s.wg.Wait()

	if s.writedRows > 0 {
		s.mx.Lock()
		s.pushDataToProcessDir()
		s.mx.Unlock()
		s.wg.Wait()
	}

	if err := s.geoip.Close(); err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}

	log.Print("ShutdownCollector END")
}
