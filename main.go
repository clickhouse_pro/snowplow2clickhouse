package main

import (
	"bitbucket.org/clickhouse_pro/components/browscap"
	"bitbucket.org/clickhouse_pro/snowplow2clickhouse/snowplow2clickhouse"
)

func main() {
	collector := snowplow2clickhouse.NewSnowplow2ClickHouse()
	defer collector.ShutdownCollector()
	collector.ParseCmdlineArgs()
	collector.CreateDataDir()
	collector.InitGeoip()
	browscap.InitBrowsCap()
	collector.InitClickHouseClient()
	collector.CreateClickHouseTables()
	collector.SpawnProcessRequest()
	collector.InitRouterAndListWebsever()
}
