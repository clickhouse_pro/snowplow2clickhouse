package snowplow2clickhouse

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/clickhouse_pro/components/stacktrace"
	"github.com/buger/jsonparser"
	"github.com/digitalcrab/browscap_go"
	"github.com/gocraft/web"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"
	"github.com/sebest/xff"
	"github.com/xeipuuv/gojsonschema"
)

// NewSnowplow2ClickHouse create new Snowplow to Clickhouse streaming structure
func NewSnowplow2ClickHouse() *Snowplow2Clickhouse {
	return &Snowplow2Clickhouse{
		schemaVersion:    1,
		formKeyExistsMap: make(map[string]bool),
		jsonKeyExistsMap: make(map[string]bool),
		dbFieldList:      make([]string, 0),
		dbToFormMap:      make(map[string]string),

		igluPrefixesMap: make(map[string]string),
		igluSchemasUrls: make(map[string]string),
		igluSchemasMap:  make(map[string]*gojsonschema.Schema),

		bufferedData: make(chan url.Values, 100),
		wg:           &sync.WaitGroup{},
		mx:           &sync.RWMutex{},
	}
}

// nolint: gocyclo
func parseJSONBody(req *web.Request, s *Snowplow2Clickhouse) {
	parseJSONEventsArray := func(event []byte, dataType jsonparser.ValueType, offset int, err error) {
		if err == nil {
			newForm := make(url.Values)
			for k, v := range req.Form {
				newForm[k] = v
			}
			parseJSONEvent := func(key []byte, value []byte, valueType jsonparser.ValueType, offset int) error {
				if valueType == jsonparser.Array || valueType == jsonparser.Object {
					return nil
				}
				newK := string(key)
				newV := string(value)

				_, simpleExists := snowplowMap[newK]
				_, complexExists := snowplowJSONMap[newK]
				if simpleExists || complexExists {
					newForm.Set(newK, newV)
				}

				switch newK {
				case "ip":
					geoipLookup(newV, newForm, s)
				case "ua":
					browsecapLookup(newV, newForm)
				case "url":
					parseUTMTokens(newV, newForm, []string{"source", "campaign", "medium", "keyword", "content"}, "utm_", "mkt_")
				case "refr":
					parseUTMTokens(newV, newForm, []string{"source", "medium", "term"}, "utm_", "refr_")
				}
				return nil
			}
			if err = jsonparser.ObjectEach(event, parseJSONEvent); err != nil {
				log.Error().Err(err)
			}

			parseJSONForm(newForm, s)

		}
	}

	if body, err := ioutil.ReadAll(req.Body); err == nil {
		schema, err := jsonparser.GetString(body, "schema")
		if err == nil && strings.Index(schema, "iglu:com.snowplowanalytics.snowplow/payload_data/") == 0 {
			if data, dataType, _, err := jsonparser.Get(body, "data"); err == nil {
				if dataType == jsonparser.Array {
					if offset, err := jsonparser.ArrayEach(data, parseJSONEventsArray); err != nil {
						log.Error().Err(err).Int("offset", offset).Msg("jsonparser.ArrayEach(data, parseJSONEventsArray) error")
					}
				}
			}
		}

	}
}

func parseJSONForm(form url.Values, s *Snowplow2Clickhouse) {
	for k := range form {
		convertBase64, spJSONExists := snowplowJSONMap[k]
		if spJSONExists {
			jsonString := form.Get(k)
			if convertBase64 {
				if jsonURLDecodedString, err := base64.RawURLEncoding.DecodeString(jsonString); err == nil {
					delete(form, k)
					s.processJSONEvents(jsonURLDecodedString, form)
				} else {
					log.Printf("k=%s base64.RawURLEncoding.DecodeString err=%s", k, err)
					log.Print(jsonString)
				}
			} else {
				delete(form, k)
				s.processJSONEvents([]byte(jsonString), form)
			}
		}
	}
	s.bufferedData <- form
}

func parseUserAgent(req *web.Request) {
	if _, uaExists := req.Form["ua"]; !uaExists {
		req.Form.Set("ua", req.UserAgent())
	}
	browsecapLookup(req.Form.Get("ua"), req.Form)
}

func browsecapLookup(uaStr string, form url.Values) {
	if ua, ok := browscap_go.GetBrowser(uaStr); ok {
		form.Set("ua_name", ua.Browser)
		form.Set("ua_v", ua.BrowserVersion)
		form.Set("os_name", ua.Platform)
		form.Set("os_v", ua.PlatformVersion)
		form.Set("device", ua.DeviceName)
		form.Set("device_t", ua.DeviceType)
	}
}

func parseGeoip(req *web.Request, s *Snowplow2Clickhouse) interface{} {
	ip := req.Form.Get("ip")
	if ip == "" {
		if req.Header.Get("X-Forwarded-For") != "" {
			ip = xff.Parse(req.Header.Get("X-Forwarded-For"))
		} else if req.Header.Get("X-Real-IP") != "" {
			ip = req.Header.Get("X-Real-IP")
		} else {
			ip = req.RemoteAddr
		}
	}
	return geoipLookup(ip, req.Form, s)
}

func geoipLookup(ip string, form url.Values, s *Snowplow2Clickhouse) interface{} {
	parsedIP := net.ParseIP(ip)
	if parsedIP != nil {
		var geoipLookup struct {
			Country struct {
				IsoCode string `maxminddb:"iso_code"`
			} `maxminddb:"country"`
			City struct {
				Names map[string]string `maxminddb:"names"`
			} `maxminddb:"city"`
			Continent struct {
				Names map[string]string `maxminddb:"names"`
			} `maxminddb:"continent"`
			Location struct {
				Latitude  float64 `maxminddb:"latitude"`
				Longitude float64 `maxminddb:"longitude"`
			} `maxminddb:"location"`
			Traits struct {
				IsAnonymousProxy bool `maxminddb:"is_anonymous_proxy"`
			} `maxminddb:"traits"`
		}

		if err := s.geoip.Lookup(parsedIP, &geoipLookup); err == nil {
			if geoipLookup.Country.IsoCode != "" {
				form.Set("geo_latt", strconv.FormatFloat(geoipLookup.Location.Latitude, 'f', 6, 64))
				form.Set("geo_long", strconv.FormatFloat(geoipLookup.Location.Longitude, 'f', 6, 64))
				form.Set("geo_country", geoipLookup.Country.IsoCode)
				form.Set("geo_city", geoipLookup.City.Names["en"])
				//TODO add "geo_asnum", "geo_provider",
				if geoipLookup.Traits.IsAnonymousProxy {
					form.Set("anonym", "1")
				} else {
					form.Set("anonym", "0")
				}
			} else {
				return nil
			}
		}
		return geoipLookup
	}
	return nil
}

func parseUTM(req *web.Request) {
	if req.Form.Get("url") != "" {
		parseUTMTokens(req.Form.Get("url"), req.Form, []string{"source", "campaign", "medium", "keyword", "content"}, "utm_", "mkt_")
	} else {
		parseUTMTokens(req.Referer(), req.Form, []string{"source", "campaign", "medium", "keyword", "content"}, "utm_", "mkt_")
	}
	parseUTMTokens(req.Form.Get("refr"), req.Form, []string{"source", "medium", "term"}, "utm_", "refr_")
}

func parseUTMTokens(refString string, form url.Values, tokens []string, sourcePrefix string, targetPrefix string) {
	if refString != "" {
		ref, err := url.Parse(refString)
		if err == nil && ref.RawQuery != "" {
			var q url.Values
			q, err = url.ParseQuery(ref.RawQuery)
			if err == nil {
				for _, k := range tokens {
					sourceK := sourcePrefix + k
					targetK := targetPrefix + k
					if form.Get(targetK) == "" && q.Get(sourceK) != "" {
						form.Set(targetK, q.Get(sourceK))
					}
				}
			} else {
				log.Printf("parseUTMTokens ERROR #1 %s", err.Error())
			}
		} else if err != nil {
			log.Printf("parseUTMTokens ERROR #2 %s", err.Error())
			log.Print(err.Error())
		}
	}

}

func formatToDbType(s string, dbType string) []byte {
	// nolint: goconst
	switch dbType {
	case "String":
		s = "'" + s + "'"
	case "Float32":
		s = formatFloat(s, 32, 4)
	case "Float64":
		s = formatFloat(s, 64, 4)
	case "Int8", "Int16", "Int32", "Int64":
		bits, err := strconv.Atoi(dbType[3:])
		if err != nil {
			s = "0"
		} else {
			s = formatInt(s, bits, true)
		}
	case "UInt8", "UInt16", "UInt32", "UInt64":
		bits, err := strconv.Atoi(dbType[4:])
		if err != nil {
			s = "0"
		} else {
			s = formatInt(s, bits, false)
		}
	case "UUID":
		guid, err := uuid.FromString(s)
		if err != nil {
			s = strings.Repeat("0", 16)
		} else {
			s = string(guid.Bytes())
		}
	}
	return []byte(s)
}

func formatFloat(s string, bytes int, precisoin int) string {
	f, err := strconv.ParseFloat(s, bytes)
	if err != nil {
		f = 0.0
	}
	s = strconv.FormatFloat(f, 'f', precisoin, bytes)
	return s
}

func formatInt(s string, bits int, sign bool) string {
	i, err := strconv.ParseInt(s, 10, bits)
	if err != nil {
		i = 0
	}
	if !sign && i < 0 {
		i = -i
	}
	s = strconv.FormatInt(i, 10)
	return s
}

//nolint: gas
func logErrorToFile(s *Snowplow2Clickhouse, rawBuffer []byte, rawErr error) {
	errorPrefix := fmt.Sprintf(
		"%s/errors/%s.%d", s.dataDir, time.Now().UTC().Format("2006-01-02__15-04-05.999999999"), os.Getpid(),
	)
	errorFile, err := os.OpenFile(errorPrefix+".log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}

	rawFile, err := os.OpenFile(errorPrefix+".raw", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err == nil {
		_, err = rawFile.Write(rawBuffer)
		if err == nil {
			_, err = errorFile.WriteString(rawErr.Error())
			if err == nil {
				err = errorFile.Close()
			}
			if err == nil {
				err = rawFile.Close()
			}
		}
	}

	if err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
}

func createDirIfNotExists(dirName string, mode os.FileMode) {
	if _, err := os.Stat(dirName); os.IsNotExist(err) {
		if err = os.Mkdir(dirName, mode); err != nil {
			log.Fatal().Err(err).Str("dirName", dirName).Uint32("mode", uint32(mode)).Msgf("error create directory")
		}
	}
}
