package snowplow2clickhouse

import (
	"log"
	"net/url"
	"testing"

	"github.com/buger/jsonparser"
	"github.com/xeipuuv/gojsonschema"
)

func TestJsonWalk(t *testing.T) {
	jsonBuffer := []byte(
		`{
			"schema": "iglu:com.snowplowanalytics.snowplow/contexts/jsonschema/1-0-0",
			"data": [
				{
					"schema": "iglu:com.google.analytics/cookies/jsonschema/1-0-0",
					"data": {
						"__utma": "1893875.2073751774.1486817701.1486884989.1486888411.3",
						"__utmb": "1893875.1.10.1486888411",
						"__utmc": "1893875",
						"__utmz": "1893875.1486817701.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)"
					}
				},
				{
					"schema": "iglu:com.snowplowanalytics.snowplow/web_page/jsonschema/1-0-0",
					"data": {
						"id": "1ceb90d5-4bd8-48f1-946f-474921a6a802"
					}
				},
				{
					"schema": "iglu:org.w3/PerformanceTiming/jsonschema/1-0-0",
					"data": {
						"navigationStart": 1486888409894,
						"unloadEventStart": 0,
						"unloadEventEnd": 0,
						"redirectStart": 0,
						"redirectEnd": 0,
						"fetchStart": 1486888410029,
						"domainLookupStart": 1486888410033,
						"domainLookupEnd": 1486888410033,
						"connectStart": 1486888410033,
						"connectEnd": 1486888410093,
						"secureConnectionStart": 0,
						"requestStart": 1486888410096,
						"responseStart": 1486888410242,
						"responseEnd": 1486888410244,
						"domLoading": 1486888410244,
						"domInteractive": 1486888410887,
						"domContentLoadedEventStart": 1486888410887,
						"domContentLoadedEventEnd": 1486888410996,
						"domComplete": 1486888411641,
						"loadEventStart": 1486888411643,
						"loadEventEnd": 1486888411648,
						"chromeFirstPaint": 1486888410746
					}
				},
				{
					"schema": "iglu:org.w3/PerformanceTiming/jsonschema/1-0-0",
					"data": {
						"navigationStart": 1486888409894,
						"unloadEventStart": 0,
						"unloadEventEnd": 0,
						"redirectStart": 0,
						"redirectEnd": 0,
						"fetchStart": 1486888410029,
						"domainLookupStart": 1486888410033,
						"domainLookupEnd": 1486888410033,
						"connectStart": 1486888410033,
						"connectEnd": 1486888410093,
						"secureConnectionStart": 0,
						"requestStart": 1486888410096,
						"responseStart": 1486888410242,
						"responseEnd": 1486888410244,
						"domLoading": 1486888410244,
						"domInteractive": 1486888410887,
						"domContentLoadedEventStart": 1486888410887,
						"domContentLoadedEventEnd": 1486888410996,
						"domComplete": 1486888411641,
						"loadEventStart": 1486888411643,
						"loadEventEnd": 1486888411648,
						"chromeFirstPaint": 1486888410746
					}
				}
			]
		}`,
	)
	s := NewSnowplow2ClickHouse()
	form := url.Values{}
	s.walkJSON(jsonBuffer, jsonparser.Object, "", form)
	log.Print(form)
}

func TestConvertSchemaNameToDbFieldName(t *testing.T) {
	s := NewSnowplow2ClickHouse()
	for k, v := range map[string]string{
		"":            "",
		"http://test": "test",
		"iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-4": "com_snowplowanalytics_snowplow_payload_data_",
	} {
		if s.convertSchemaNameToPrefix(k) != v {
			t.Fatalf(`s.convertSchemaNameToPrefix(%s)=%s != %s`, k, s.convertSchemaNameToPrefix(k), v)
		}
	}

}

func TestSnowplow2Clickhouse_ValidateJSONSchema(t *testing.T) {
	schemaContent := `
	{
		"$schema": "http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#",
		"description": "Schema for Google Analytics Enhanced Ecommerce action data",
		"self": {
			"vendor": "com.google.analytics.enhanced-ecommerce",
			"name": "actionFieldObject",
			"format": "jsonschema",
			"version": "1-0-0"
		},

		"type": "object",
		"properties": {
			"id": {
				"type": "string",
				"maxLength": 500
			},
			"affiliation": {
				"type": "string",
				"maxLength": 500
			},
			"revenue": {
				"type": "number",
				"multipleOf": 0.01
			},
			"tax": {
				"type": "number",
				"multipleOf": 0.01
			},
			"shipping": {
				"type": "number",
				"multipleOf": 0.01
			},
			"coupon": {
				"type": "string",
				"maxLength": 500
			},
			"list": {
				"type": "string",
				"maxLength": 500
			},
			"step": {
				"type": "integer",
				"minimum": 0,
				"maximum": 2147483647
			},
			"option": {
				"type": "string",
				"maxLength": 500
			},
			"currency": {
				"type": "string",
				"maxLength": 3,
				"minLength": 3
			}
		},
		"additionalProperties": false
	}
	`
	jsonContent := `
	{"data":{
			"id": "T2",
			"affiliation": "Cloudpayments",
			"revenue": 19.99,
			"tax": 2.85,
			"shipping": 5.34,
			"coupon": "COUPON_EXAMPLE"
	}}
	`

	schema, err := gojsonschema.NewSchema(gojsonschema.NewStringLoader(schemaContent))
	if err == nil {
		s := NewSnowplow2ClickHouse()
		err = s.validateJSONSchema([]byte(jsonContent), "http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#", schema)
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal(err)
	}
}
