package snowplow2clickhouse

import (
	"fmt"
	"github.com/roistat/go-clickhouse"
	"log"
)

func (s *Snowplow2Clickhouse) createClickhouseFieldIfNotExists(dbField, dbType string) {
	s.mx.Lock()
	defer s.mx.Unlock()
	if _, jExists := s.jsonKeyExistsMap[dbField]; !jExists {
		for _, t := range []string{"events_local", "events_distributed"} {
			q := clickhouse.NewQuery(getCurrentFieldsQuery, s.database, s.tablePrefix+t, dbField)
			lenCon := len(s.clickhouseConns)
			for i := 0; i < lenCon; i++ {
				if rs := q.Iter(s.clickhouseConns[i]); rs.Error() == nil {
					var existsType string
					var alterQ clickhouse.Query
					var err error

					if rs.Scan(&existsType) {
						if existsType != dbType {
							alterQ = clickhouse.NewQuery(fmt.Sprintf(alterFieldTypeQuery, s.database, s.tablePrefix+t, dbField, dbType))
						}
					} else {
						alterQ = clickhouse.NewQuery(fmt.Sprintf(addFieldQuery, s.database, s.tablePrefix+t, dbField, dbType))
					}

					if alterQ.Stmt != "" {
						if err = alterQ.Exec(s.clickhouseConns[i]); err != nil {
							log.Printf("createClickhouseFieldIfNotExists Stmt=%s dbField=%s err.Error()=%s", alterQ.Stmt, dbField, err.Error())
						}
					}
				}
			}
		}
		s.jsonKeyExistsMap[dbField] = false
	}
}
