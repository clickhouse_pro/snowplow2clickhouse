package snowplow2clickhouse

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/buger/jsonparser"
	"github.com/rs/zerolog/log"
	"github.com/xeipuuv/gojsonschema"
)

func (s *Snowplow2Clickhouse) fillIgluPrefixesMap() {
	s.igluPrefixes = strings.Join(defaultIgluPrefixes, "=http://iglucentral.com/schemas/,") + "=http://iglucentral.com/schemas/," + s.igluPrefixes
	for _, p := range strings.Split(s.igluPrefixes, ",") {
		x := strings.Split(p, "=")
		if len(x) == 2 {
			s.igluPrefixesMap[x[0]] = x[1]
		}
	}
}

func (s *Snowplow2Clickhouse) validateJSONSchema(jsonBuffer []byte, schemaURL string, schema *gojsonschema.Schema) (err error) {
	data, _, _, err := jsonparser.Get(jsonBuffer, "data")
	if err != nil {
		return err
	}
	result, err := schema.Validate(gojsonschema.NewStringLoader(string(data)))
	if err != nil {
		err = fmt.Errorf("Validation error schemaURL=%s err=%s", schemaURL, err)
		logErrorToFile(s, jsonBuffer, err)
		return err
	}
	if !result.Valid() {
		validationErrors := result.Errors()
		res := make([]string, 1+len(validationErrors))
		res[0] = fmt.Sprintf("schemaURL=%s", schemaURL)
		for i, err := range validationErrors {
			res[1+i] = fmt.Sprintf(
				"err.Type()=%s err.Value()=%v err.Context()=%s err.Description()=%s err.Details()=%s",
				err.Type(), err.Value(), err.Context().String(), err.Description(), err.Details(),
			)
		}
		err = fmt.Errorf("JSON Validation errors %s", "\n"+strings.Join(res, "\n- ")+"\n")
		logErrorToFile(s, data, err)
		return err
	}
	return nil
}

func (s *Snowplow2Clickhouse) getIgluSchema(jsonBuffer []byte) (schemaName string, schemaURL string, schema *gojsonschema.Schema, err error) {
	schemaName, err = jsonparser.GetString(jsonBuffer, "schema")
	if err != nil {
		err = fmt.Errorf("schema not exists in jsonBuffer=%s err=%v", jsonBuffer, err)
		return "", "", nil, err
	}
	s.mx.RLock()
	schema, sExists := s.igluSchemasMap[schemaName]
	schemaURL, uExists := s.igluSchemasUrls[schemaName]
	s.mx.RUnlock()
	if !sExists || !uExists {
		if strings.Index(schemaName, "iglu:") == 0 {
			schemaURL, err = s.getFullURLByIgluPrefix(schemaName)
			if err != nil {
				return "", "", nil, err
			}
		} else if strings.Index(schemaName, "http://") == 0 || strings.Index(schemaName, "https://") == 0 {
			schemaURL = schemaName
		} else {
			return "", "", nil, errors.New("Invalid schemaName=" + schemaName)
		}
		schema, err = gojsonschema.NewSchema(gojsonschema.NewReferenceLoader(schemaURL))
		if err != nil {
			log.Printf("NewSchema error schemaURL=%s err=%s", schemaURL, err.Error())
			return "", "", nil, err
		}
		s.mx.Lock()
		s.igluSchemasMap[schemaName] = schema
		s.igluSchemasUrls[schemaName] = schemaURL
		s.mx.Unlock()
		return schemaName, schemaURL, schema, nil
	}
	return schemaName, schemaURL, schema, nil
}

func (s *Snowplow2Clickhouse) getFullURLByIgluPrefix(schemaName string) (fullURL string, err error) {
	found := 0
	for prefix, prefixURL := range s.igluPrefixesMap {
		if strings.Index(schemaName, "iglu:"+prefix) == 0 && found < len(prefix) {
			found = len(prefix)
			fullURL = prefixURL + schemaName[5:]
		}
	}
	if found > 0 {
		return fullURL, nil
	}
	return "", fmt.Errorf("schemaName=%s not found in igluPrefixesMap=%v", schemaName, s.igluPrefixesMap)
}

func (s *Snowplow2Clickhouse) convertSchemaNameToPrefix(schemaName string) (dbName string) {
	l := len(schemaName)
	if l > 5 && schemaName[:5] == "iglu:" {
		dbName = schemaName[5:]
	} else if l > 7 && schemaName[:7] == "http://" {
		dbName = schemaName[7:]
	} else if l > 8 && schemaName[:8] == "https://" {
		dbName = schemaName[8:]
	} else {
		dbName = schemaName
	}
	dbName = string(regexpJSONSchemaVersion.ReplaceAll([]byte(dbName), []byte("")))
	dbName = strings.Replace(dbName, "/jsonschema", "_", -1)
	dbName = string(regexpNotAZAndNum.ReplaceAll([]byte(dbName), []byte("_")))
	return dbName
}

// nolint: gocyclo, goconst
func (s *Snowplow2Clickhouse) walkJSON(jsonBuffer []byte, dataType jsonparser.ValueType, dbField string, form url.Values) (err error) {
	walkJSONArray := func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) (err error) {
		return s.walkJSON(value, dataType, dbField+"_"+string(key), form)
	}

	var dbType, dbValue string
	// nolint: goconst
	switch dataType {
	case jsonparser.Object:
		if err = jsonparser.ObjectEach(jsonBuffer, walkJSONArray); err != nil {
			return err
		}
		return nil
	case jsonparser.Array:
		var i int
		if _, err = jsonparser.ArrayEach(jsonBuffer, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			if err == nil {
				if err = s.walkJSON(value, dataType, dbField+"_"+strconv.Itoa(i), form); err != nil {
					log.Error().Err(err).Msg("jsonparser.ArrayEach -> s.walkJSON error")
				}
				i++
			}
		}); err != nil {
			return err
		}
		return nil
	case jsonparser.String:
		dbValue = string(jsonBuffer)
		dbType = "String"
	case jsonparser.Number:
		dbValue = string(jsonBuffer)
		dbType = "Float64"
	case jsonparser.Boolean:
		dbValue = string(jsonBuffer)
		dbType = "UInt8"
		if strings.ToLower(dbValue) == "true" {
			dbValue = "1"
		} else {
			dbValue = "0"
		}
	case jsonparser.Null:
		dbType = "String"
		dbValue = ""
	}

	dbField = string(regexpNotAZAndNum.ReplaceAll([]byte(dbField), []byte("_")))
	s.createClickhouseFieldIfNotExists(dbField, dbType)

	form.Set(dbField, dbValue)

	return nil
}
