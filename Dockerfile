FROM golang:alpine AS builder
MAINTAINER Eugene Klimov <bloodjazman@gmail.com>

WORKDIR /snowplow2clickhouse/
VOLUME /snowplow2clickhouse/data

ENV GOPATH /snowplow2clickhouse/

COPY ./.gometalinter.json /snowplow2clickhouse/
COPY ./snowplow2clickhouse/ /snowplow2clickhouse/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse/snowplow2clickhouse/
COPY ./cmd/ /snowplow2clickhouse/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse/cmd/
COPY ./id_rsa /root/.ssh/id_rsa
RUN chmod 0600 /root/.ssh/id_rsa

RUN apk add --no-cache --update git openssh-client
RUN touch /root/.ssh/known_hosts
RUN ssh-keygen -R github.com
RUN ssh-keygen -R bitbucket.org
RUN ssh-keyscan -H github.com >> /root/.ssh/known_hosts
RUN ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts
RUN git config --global url."git@github.com:".insteadOf "https://github.com/"
RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"


RUN mkdir -m 0755 -p /var/lib/GeoIP/
ADD http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz /var/lib/GeoIP/GeoLite2-City.mmdb.gz
RUN gzip -d /var/lib/GeoIP/GeoLite2-City.mmdb.gz


RUN mkdir -p /snowplow2clickhouse/bin \
    && cd /snowplow2clickhouse/ \
    && go get -v ./src/bitbucket.org/clickhouse_pro/snowplow2clickhouse/snowplow2clickhouse/...

RUN go get -u github.com/alecthomas/gometalinter
RUN /snowplow2clickhouse/bin/gometalinter --install
RUN /snowplow2clickhouse/bin/gometalinter --config=/snowplow2clickhouse/.gometalinter.json --exclude=/go/src/ /snowplow2clickhouse/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse/...

RUN go test -v bitbucket.org/clickhouse_pro/snowplow2clickhouse/snowplow2clickhouse \
    && go build -o /snowplow2clickhouse/bin/snowplow2clickhouse /snowplow2clickhouse/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse/main.go \
    && rm -rf /snowplow2clickhouse/src


FROM alpine:latest
ENV DOCKERIZE_VERSION v0.4.0
RUN apk add --no-cache --update ca-certificates openssl git \
		&& apk add --no-cache --update gzip sed \
    && update-ca-certificates \
    && wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY --from=builder /snowplow2clickhouse/bin/snowplow2clickhouse /snowplow2clickhouse/bin/
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
COPY --from=builder /var/lib/GeoIP/GeoLite2-City.mmdb /var/lib/GeoIP/GeoLite2-City.mmdb
RUN echo "wget -O - http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz | gzip -d > /var/lib/GeoIP/GeoLite2-City.mmdb" > /etc/periodic/weekly/geoiplite2

ENTRYPOINT ["/bin/sh","-c"]