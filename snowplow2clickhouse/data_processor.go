package snowplow2clickhouse

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"errors"
	"net/http"
	"net/url"
	"os"
	"strings"

	"bitbucket.org/clickhouse_pro/components/stacktrace"
	"github.com/rs/zerolog/log"

	"fmt"

	"github.com/buger/jsonparser"
	"github.com/roistat/go-clickhouse"
)

//SpawnProcessRequest initialize goroutine who receive all requests data
func (s *Snowplow2Clickhouse) SpawnProcessRequest() {
	log.Print("spawnProcessRequest BEGIN")
	s.wg.Add(1)
	go s.processRequest()
	log.Print("spawnProcessRequest END")
}

// nolint: gocyclo
func (s *Snowplow2Clickhouse) processRequest() {
	log.Print("processRequest BEGIN")
	defer s.wg.Done()

	for form := range s.bufferedData {
		refreshSchema := false
		s.mx.Lock()
		for k := range form {
			_, fExists := s.formKeyExistsMap[k]
			jsonRefresh, jExists := s.jsonKeyExistsMap[k]
			_, spExists := snowplowMap[k]

			if !fExists && spExists {
				refreshSchema = refreshCSVShemaWithFormKey(s, k) || refreshSchema
			}

			if !jsonRefresh && jExists {
				refreshSchema = refreshCSVShemaWithJSONKey(s, k) || refreshSchema
			}
		}
		if refreshSchema {
			if s.writedRows > 0 {
				s.pushDataToProcessDir()
				s.schemaVersion++
			}
			s.initCSVWriter()
			s.writeCSVHead()
		}

		s.writeCSVRow(form)

		if s.writedRows > 0 && (s.writedRows%s.chunkSize) == 0 {
			s.pushDataToProcessDir()
			s.initCSVWriter()
			s.writeCSVHead()
		}
		s.mx.Unlock()
	}
	log.Print("processRequest END after channel closed")
}

func (s *Snowplow2Clickhouse) processJSONEvents(jsonEvents []byte, form url.Values) {
	schemaName, schemaURL, schema, err := s.getIgluSchema(jsonEvents)
	if err == nil {
		err = s.validateJSONSchema(jsonEvents, schemaURL, schema)
		if err == nil {
			var data []byte
			var dataType jsonparser.ValueType
			data, dataType, _, err = jsonparser.Get(jsonEvents, "data")
			if err == nil {
				if dataType == jsonparser.Array {
					var offset int
					offset, err = jsonparser.ArrayEach(data, func(event []byte, eventType jsonparser.ValueType, offset int, err error) {
						if err == nil {
							s.processJSONEvents(event, form)
						}
					})
					if err != nil {
						log.Error().Err(err).Int("offset", offset).Msg("jsonparser.ArrayEach error")
					}

				} else if dataType == jsonparser.Object {
					err = s.walkJSON(data, dataType, s.convertSchemaNameToPrefix(schemaName), form)
				}
			}
		}
	}

	if err != nil {
		logErrorToFile(s, jsonEvents, err)
	}
}

func (s *Snowplow2Clickhouse) pushDataToProcessDir() {
	log.Print("pushDataToProcessDir BEGIN")

	s.flushAllDataAndCloseDesciptors()
	processCsvFileName := strings.Replace(s.csvFileName, "/collect/", "/process/", 1)
	if err := os.Rename(s.csvFileName, processCsvFileName); err == nil {
		log.Printf("%s -> %s", s.csvFileName, processCsvFileName)
		s.writedRows = 0
		if !s.clickhouse.IsDown() {
			s.wg.Add(1)
			go s.batchInsertCsvToClickHouse(processCsvFileName)
		} else {
			stacktrace.DumpErrorStackAndExit(errors.New("clickhouse cluster is down"))
		}
	} else {
		stacktrace.DumpErrorStackAndExit(err)
	}
	log.Print("pushDataToProcessDir END")
}

func (s *Snowplow2Clickhouse) flushAllDataAndCloseDesciptors() {
	log.Print("flushAllDataAndCloseDesciptors BEGIN")

	if err := s.csvFile.Close(); err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
	log.Print("flushAllDataAndCloseDesciptors END")
}

// TODO refactoring to use zlib compression handler
func (s *Snowplow2Clickhouse) batchInsertCsvToClickHouse(csvFileName string) {
	defer s.wg.Done()
	log.Printf("batchInsertCsvToClickHouse BEGIN csvFileName=%s", csvFileName)

	conn := s.clickhouse.ActiveConn()
	if conn == nil {
		log.Error().Str("csvFileName", "csvFileName").Msg("currently no live ClickHouse connections available")
		return
	}

	err := s.uploadCsvStreamToClickHouse(csvFileName, conn)

	if err != nil {
		log.Printf("Error csvFileName=%s %s", csvFileName, err.Error())
		return
	}
	archiveCsvFileName := strings.Replace(csvFileName, "/process/", "/archive/", 1)
	err = os.Rename(csvFileName, archiveCsvFileName)
	if err == nil {
		err = gzipCsvFile(archiveCsvFileName)
		if err == nil {
			err = os.Remove(archiveCsvFileName)
		}
	}
	if err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
	log.Printf("%s -> %s.gz", csvFileName, archiveCsvFileName)
	log.Print("batchInsertCsvToClickHouse END")
}
func (s *Snowplow2Clickhouse) uploadCsvStreamToClickHouse(csvFileName string, conn *clickhouse.Conn) error {
	var resp *http.Response
	var req *http.Request

	csvFile, err := os.Open(csvFileName)
	if err == nil {
		req, err = http.NewRequest("POST", conn.Host, csvFile)
		if err == nil {
			req.Header.Set("Content-Type", "text/plain")
			client := http.Client{}
			resp, err = client.Do(req)
			if err == nil {
				if resp.StatusCode != http.StatusOK {
					buf := bytes.Buffer{}
					_, err = buf.ReadFrom(resp.Body)
					if err == nil {
						stacktrace.DumpErrorStackAndExit(fmt.Errorf("csvFileName=%v resp.StatusCode=%v responseBody=%v", csvFileName, resp.StatusCode, buf.String()))
					} else {
						stacktrace.DumpErrorStackAndExit(fmt.Errorf("csvFileName=%v resp.StatusCode=%v responseBodyReadError=%v", csvFileName, resp.StatusCode, err))
					}
				}
				err = resp.Body.Close()
			}
		}
	}
	return err
}

func gzipCsvFile(archiveCsvFileName string) error {
	var err error
	var archiveCsvFile, archiveGzFile *os.File
	var readedBytes int
	var info os.FileInfo
	const kb8 = 1024 * 8

	archiveCsvFile, err = os.Open(archiveCsvFileName)
	if err == nil {
		archiveGzFile, err = os.Create(archiveCsvFileName + ".gz")
		if err == nil {
			info, err = archiveCsvFile.Stat()
			if err == nil {
				chunkBytes := make([]byte, kb8)
				buffer := bufio.NewReader(archiveCsvFile)
				size := info.Size()
				var i int64
				gzWriter := gzip.NewWriter(archiveGzFile)
				for i = 0; i < size; i += kb8 {
					readedBytes, err = buffer.Read(chunkBytes)
					if err == nil {
						_, err = gzWriter.Write(chunkBytes[:readedBytes])
					}

					if err != nil {
						break
					}
				}
				if err = gzWriter.Close(); err != nil {
					return err
				}
			}
		}

	}

	_ = archiveCsvFile.Close()
	_ = archiveGzFile.Close()
	return err
}
