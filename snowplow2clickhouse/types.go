package snowplow2clickhouse

import (
	"net/url"
	"os"
	"regexp"
	"sync"

	"github.com/oschwald/maxminddb-golang"
	"github.com/roistat/go-clickhouse"
	"github.com/xeipuuv/gojsonschema"
)

//Snowplow2Clickhouse main streaming Snowplow to Clickhouse structure
type Snowplow2Clickhouse struct {
	mx               *sync.RWMutex
	wg               *sync.WaitGroup
	dataDir          string
	listen           string
	database         string
	tablePrefix      string
	clusterName      string
	formKeyExistsMap map[string]bool
	jsonKeyExistsMap map[string]bool
	dbToFormMap      map[string]string
	dbFieldList      []string
	bufferedData     chan url.Values
	clickhouse       *clickhouse.Cluster
	clickhouseConns  []*clickhouse.Conn
	clickhouseHosts  []string
	csvFile          *os.File
	csvFileName      string
	geoipDir         string
	igluPrefixes     string
	igluPrefixesMap  map[string]string
	igluSchemasUrls  map[string]string
	igluSchemasMap   map[string]*gojsonschema.Schema
	geoip            *maxminddb.Reader
	writedRows       uint
	chunkSize        uint
	dropDatabase     bool
	schemaVersion    uint16
}

// see https://github.com/snowplow/iglu-central/tree/master/schemas
var defaultIgluPrefixes = []string{
	"com.amazon",
	"com.callrail",
	"com.clearbit",
	"com.google",
	"com.hipchat",
	"com.mailchimp",
	"com.mandrill",
	"com.mparticle",
	"com.optimizely",
	"com.pagerduty",
	"com.parrable",
	"com.pingdom",
	"com.segment",
	"com.sendgrid",
	"com.snowplow",
	"com.twitter",
	"com.urbanairship",
	"io.augur",
	"io.redash",
	"org.ietf",
	"org.openweathermap",
	"org.schema",
	"org.w3",
}

// see https://github.com/snowplow/snowplow/wiki/snowplow-tracker-protocol
var snowplowMap = map[string]string{

	"tna": "name_tracker",
	"aid": "app_id",
	"p":   "platform",

	"dtm": "dvce_created_tstamp",
	"stm": "dvce_sent_tstamp",
	"ttm": "true_tstamp",
	"tz":  "os_timezone",

	"e":   "event",
	"tid": "txn_id",
	"eid": "event_id",

	"tv": "v_tracker",

	"duid":  "domain_userid",
	"nuid":  "network_userid",
	"tnuid": "network_userid",
	"uid":   "user_id",

	"vid": "domain_sessionidx",
	"sid": "domain_sessionid",

	"ip": "user_ipaddress",

	"res": "screen_resolution",
	"url": "page_url",

	"ua": "useragent",

	"page":  "page_title",
	"refr":  "page_referrer",
	"fp":    "user_fingerprint",
	"ctype": "connection_type",

	"cookie":  "br_cookie",
	"lang":    "br_lang",
	"f_pdf":   "br_features_pdf",
	"f_qt":    "br_features_quicktime",
	"f_realp": "br_features_realplayer",
	"f_wma":   "br_features_windowsmedia",
	"f_dir":   "br_features_director",
	"f_fla":   "br_features_flash",
	"f_java":  "br_features_java",
	"f_gears": "br_features_gears",
	"f_ag":    "br_features_silverlight",

	"cd": "br_colordepth",
	"ds": "doc_resolution",
	"cs": "doc_charset",

	"vp":  "br_viewport",
	"mac": "mac_address",

	"pp_mix": "pp_xoffset_min",
	"pp_max": "pp_xoffset_max",
	"pp_miy": "pp_yoffset_min",
	"pp_may": "pp_yoffset_max",

	// see https://github.com/snowplow/snowplow/wiki/snowplow-tracker-protocol#34-ad-impression-tracking
	"ad_ca": "mkt_campaign",
	"ad_ba": "mkt_content",
	"ad_ad": "mkt_source",

	// see https://github.com/snowplow/snowplow/wiki/snowplow-tracker-protocol#35-ecommerce-tracking
	"tr_id": "tr_orderid",
	"tr_af": "tr_affiliation",
	"tr_tt": "tr_total",
	"tr_tx": "tr_tax",
	"tr_sh": "tr_shipping",
	"tr_ci": "tr_city",
	"tr_st": "tr_state",
	"tr_co": "tr_country",
	"tr_cu": "tr_currency",

	"ti_id": "ti_orderid",
	"ti_sk": "ti_sku",
	"ti_na": "ti_name",
	"ti_ca": "ti_category",
	"ti_pr": "ti_price",
	"ti_qu": "ti_quantity",
	"ti_cu": "ti_currency",

	// see https://github.com/snowplow/snowplow/wiki/snowplow-tracker-protocol#39-custom-structured-event-tracking
	"se_ca": "se_category",
	"se_ac": "se_action",
	"se_la": "se_label",
	"se_pr": "se_property",
	"se_va": "se_value",

	// geoIp not Snowplow compatible fields
	"geo_latt":    "geo_latitude",
	"geo_long":    "geo_longitude",
	"geo_region":  "geo_region",
	"geo_country": "geo_country",
	"geo_city":    "geo_city",
	"anonym":      "is_anonymous",
	//	"geo_asnum":    "geo_asnum",
	//	"geo_provider": "geo_provider",

	// userAgent not Snowplow compatible fields
	"ua_name":  "browser_name",
	"ua_v":     "browser_version",
	"os_name":  "os",
	"os_v":     "os_version",
	"device":   "device",
	"device_t": "device_type",

	// UTM parsing not Snowplow compatible fields
	"utm_campaign": "mkt_campaign",
	"utm_source":   "mkt_source",
	"utm_medium":   "mkt_medium",
	"utm_keyword":  "mkt_keyword",
	"utm_content":  "mkt_content",

	"refr_medium": "refr_medium",
	"refr_source": "refr_source",
	"refr_term":   "refr_term",
}

// see https://github.com/snowplow/snowplow/wiki/snowplow-tracker-protocol#310-custom-unstructured-event-tracking
// see https://github.com/snowplow/snowplow/wiki/snowplow-tracker-protocol#4-custom-contexts
// each value describe use base64 decode for json or not
var snowplowJSONMap = map[string]bool{
	"ue_pr": false,
	"ue_px": true,
	"co":    false,
	"cx":    true,
}

var dbFieldTypes = map[string]string{
	"dvce_sent_tstamp":         "UInt32",
	"dvce_created_tstamp":      "UInt32",
	"true_tstamp":              "UInt32",
	"txn_id":                   "UInt64",
	"event_id":                 "UUID",
	"domain_sessionidx":        "UInt32",
	"domain_sessionid":         "UUID",
	"geo_latitude":             "Float32",
	"geo_longitude":            "Float32",
	"is_anonymous":             "UInt8",
	"user_fingerprint":         "UInt64",
	"br_cookie":                "UInt8",
	"br_features_pdf":          "UInt8",
	"br_features_quicktime":    "UInt8",
	"br_features_realplayer":   "UInt8",
	"br_features_windowsmedia": "UInt8",
	"br_features_director":     "UInt8",
	"br_features_flash":        "UInt8",
	"br_features_java":         "UInt8",
	"br_features_gears":        "UInt8",
	"br_features_silverlight":  "UInt8",
	"pp_xoffset_min":           "UInt32",
	"pp_xoffset_max":           "UInt32",
	"pp_yoffset_min":           "UInt32",
	"pp_yoffset_max":           "UInt32",
	"tr_total":                 "Float32",
	"tr_tax":                   "Float32",
	"tr_shipping":              "Float32",
	"ti_price":                 "Float32",
	"ti_quantity":              "Float32",
	"se_value":                 "Float32",
}

var createMainEventsTableTemplate = `CREATE TABLE IF NOT EXISTS %s (

		name_tracker String,
		app_id String,
		platform String,

		dvce_sent_tstamp UInt32,
		date_sent MATERIALIZED toDate(dvce_created_tstamp),
		dvce_created_tstamp UInt32,
		date_created MATERIALIZED toDate(dvce_created_tstamp),
		true_tstamp UInt32,
		os_timezone String,

		event String,
		txn_id UInt64,
		event_id FixedString(16),
		event_uuid ALIAS UUIDNumToString(event_id),

		v_tracker String,
		domain_userid String,
		network_userid String,
		user_id String,

		domain_sessionidx UInt32,
		domain_sessionid FixedString(16),
		domain_sessionid_uuid ALIAS UUIDNumToString(event_id),

		mac_address String,
		user_ipaddress String,
		geo_latitude Float32,
		geo_longitude Float32,
		geo_region_name String,
		geo_country String,
		geo_city String,
		is_anonymous UInt8,

		screen_resolution String,
		dvce_screenwidth ALIAS extract(screen_resolution, '^(\d+)x'),
		dvce_screenheight ALIAS extract(screen_resolution, 'x(\d+)$'),

		page_url String,
		page_title String,
		page_referrer String,

		useragent String,
		browser_name String,
		browser_version String,
		os String,
		os_version String,
		device String,
		device_type String,

		user_fingerprint UInt64,
		connection_type String,

		br_cookie UInt8,
		br_lang String,

		br_features_pdf UInt8,
		br_features_quicktime UInt8,
		br_features_realplayer UInt8,
		br_features_windowsmedia UInt8,
		br_features_director UInt8,
		br_features_flash UInt8,
		br_features_java UInt8,
		br_features_gears UInt8,
		br_features_silverlight UInt8,

		doc_resolution String,
		doc_width ALIAS extract(doc_resolution, '^(\d+)x'),
		doc_height ALIAS extract(doc_resolution, 'x(\d+)$'),

		doc_charset String,

		br_colordepth FixedString(12),
		br_viewport String,
		br_viewwidth ALIAS extract(br_viewport, '^(\d+)x'),
		br_viewheight ALIAS extract(br_viewport, 'x(\d+)$'),

		pp_xoffset_min UInt32,
		pp_xoffset_max UInt32,
		pp_yoffset_min UInt32,
		pp_yoffset_max UInt32,

		mkt_campaign String,
		mkt_source String,
		mkt_medium String,
		mkt_keyword String,
		mkt_content String,

		refr_medium String,
		refr_source String,
		refr_term String,

		tr_orderid String,
		tr_affiliation String,
		tr_total Float32,
		tr_tax Float32,
		tr_shipping Float32,
		tr_city String,
		tr_state String,
		tr_country String,
		tr_currency FixedString(3),

		ti_orderid String,
		ti_sku String,
		ti_name String,
		ti_category String,
		ti_price Float32,
		ti_quantity Float32,
		ti_currency FixedString(3),

		se_category String,
		se_action String,
		se_label String,
		se_property String,
		se_value Float32

) %s`

var getCurrentFieldsQuery = `SELECT type FROM system.columns WHERE database=? AND table=? AND name=?`
var alterFieldTypeQuery = `ALTER TABLE %s.%s MODIFY COLUMN %s %s`
var addFieldQuery = `ALTER TABLE %s.%s ADD COLUMN %s %s`

var regexpNotAZAndNum = regexp.MustCompile(`[^a-zA-Z0-9]+`)
var regexpJSONSchemaVersion = regexp.MustCompile(`/[0-9\-]+$`)
