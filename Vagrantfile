Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/xenial64"
  config.vm.box_check_update = false

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false

    # Customize the amount of memory on the VM:
    vb.memory = "2048"
  end

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = false

  config.vm.define :snowplow2clickhouse_develop do |snowplow2clickhouse_develop|
    snowplow2clickhouse_develop.vm.network "private_network", ip: "172.16.1.99"
    snowplow2clickhouse_develop.vm.synced_folder "./geoip", "/var/lib/GeoIP"
    snowplow2clickhouse_develop.vm.synced_folder "./", "/home/ubuntu/go/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse"
    snowplow2clickhouse_develop.vm.synced_folder "../components", "/home/ubuntu/go/src/bitbucket.org/clickhouse_pro/components"

    snowplow2clickhouse_develop.vm.host_name = "vagrant-snowplow2clickhouse-clickhouse-pro"
    snowplow2clickhouse_develop.hostmanager.aliases = ["vagrant.snowplow2clickhouse.clickhouse.pro"]
    snowplow2clickhouse_develop.vm.provision "shell", inline: <<-SHELL
			set -xeuo pipefail
			echo 'APT::Periodic::Enable "0";' > /etc/apt/apt.conf.d/02periodic
			export DEBIAN_FRONTEND=noninteractive
			export GOPATH=/home/ubuntu/go/
			grep -q -F 'export GOPATH=$GOPATH' /home/ubuntu/.bashrc  || echo "export GOPATH=$GOPATH" >> /home/ubuntu/.bashrc
			grep -q -F 'export GOPATH=$GOPATH' /home/vagrant/.bashrc || echo "export GOPATH=$GOPATH" >> /home/vagrant/.bashrc
			grep -q -F 'export GOPATH=$GOPATH' /root/.bashrc         || echo "export GOPATH=$GOPATH" >> /root/.bashrc
			# sysctl net.ipv6.conf.all.forwarding=1
			apt-get update
			apt-get install -y apt-transport-https software-properties-common aptitude
			# clickhouse
			apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E0C56BD4
			# gophers
			apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 136221EE520DDFAF0A905689B9316A7BC7917B12
			# docker
			apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8D81803C0EBFCD88
			add-apt-repository "deb https://download.docker.com/linux/ubuntu xenial edge"
			add-apt-repository "deb http://repo.yandex.ru/clickhouse/xenial/ stable main"
			add-apt-repository ppa:gophers/archive
			apt-get update
			apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual
			apt-get install -y golang-1.9
			apt-get install -y docker-ce
			apt-get install -y htop ethtool mc iotop
			apt-get install -y python-pip
			apt-get install -y clickhouse-server-common clickhouse-client
			apt-get install -y geoipupdate
			python -m pip install -U pip
			pip install -U docker-compose

			ln -nvsf /usr/lib/go-1.9/bin/go /bin/go
			ln -nvsf /usr/lib/go-1.9/bin/gofmt /bin/gofmt


			echo "UserId 999999\nLicenseKey 000000000000\nProductIds GeoLite2-City GeoLite2-Country" > /etc/GeoIP.conf
			echo "1 13 * * 5 root /usr/bin/geoipupdate" > /etc/cron.d/geoipupdate_cron
			geoipupdate -v

			mkdir -p -m 0700 /root/.ssh/
			cp -fv /vagrant/id_rsa /root/.ssh/id_rsa
			chmod 0600 /root/.ssh/id_rsa
			touch /root/.ssh/known_hosts
			ssh-keygen -R github.com
			ssh-keygen -R bitbucket.org
			ssh-keyscan -H github.com >> /root/.ssh/known_hosts
			ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts

			git config --global url."git@github.com:".insteadOf "https://github.com/"
			git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
			bash -c "cd /home/ubuntu/go/src/bitbucket.org/clickhouse_pro/snowplow2clickhouse && go get -v -d ./..."

			systemctl restart clickhouse-server

			set +x
			echo "========================================"
			echo " snowplow2clickhouse VAGRANT PROVISIONING DONE"
			echo "========================================"
			echo " "
			echo "use folloding scenario for developing:"
			echo "#  sudo bash -c \\\"cd /vagrant && GOPATH=/home/ubuntu/go go run main.go\\\""
			echo " "
			echo "use folloding scenario for local docker images push:"
			echo "#  vagrant ssh snowplow2clcikhouse_develop"
			echo "#  sudo bash -c \\\"cd /vagrant && ./docker-publisher.sh\\\""
			echo " "
			echo "javascript example: "
			echo "#  open in your browser http://vagrant.snowplow2clickhouse.clickhouse.pro/analytics_js_example.html"
			echo " "
			echo "=============="
			echo " Good Luck ;) "
			echo "=============="
    SHELL
    end
end
